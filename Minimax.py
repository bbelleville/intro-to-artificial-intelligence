def minimaxValue(gameState, depth, maximizingPlayer):
    # gameState: game state to be evaluated
    # depth: search depth
    # maximizingPlayer: True if maximizing player's turn

    # return: value of game state
    
    # tracking number of nodes examined
    global nodesExamined
    nodesExamined += 1
    
    # Recursion has reached the search depth or a terminal node
    if depth == 0:
        return gameState.evaluate()

    children = gameState.getChildren()
    if children == []:
        return gameState.evaluate()

    if maximizingPlayer:
        bestVal = -float('inf')
        for child in children:
            val = minimaxValue(child, depth-1, False)
            
            bestVal = max(bestVal, val)
        return bestVal
    else:
        bestVal = float('inf')
        for child in children:
            val = minimaxValue(child, depth-1, True)

            bestVal = min(bestVal, val)
        return bestVal

def minimaxDecision(gameState, depth):
    # gameState: current game state
    # depth: search depth
    
    # return: gameState with highest value
    
    if depth < 1:
        raise Exception('Depth must be at least 1.')
    
    global nodesExamined
    nodesExamined = 0

    bestValue = -float('inf')
    bestMove = None
    
    children = gameState.getChildren()
    
    for child in children:
        val = minimaxValue(child, depth-1, False)

        # By testing if bestMove is true, this will always select a
        # move, even when none are better than -inf. It shouldn't
        # change any decisions, except for the cases where all moves
        # have value -inf.
        if not bestMove or val > bestValue:
            bestMove = child
            bestValue = val
            
    return bestMove, nodesExamined
        
    
