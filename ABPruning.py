def alphabetaValue(gameState, depth, alpha, beta, maximizingPlayer):
    # gameState: game state to be evaluated
    # depth: search depth
    # alpha: maximum score, initially -Inf
    # beta: minimum score, initiall Inf
    # maximizingPlayer: True if maximizing player's turn

    # return: value of game state
    
    # tracking number of nodes examined
    global nodesExamined
    nodesExamined += 1
    
    # Recursion has reached the search depth or a terminal node
    if depth == 0:
        return gameState.evaluate()

    children = gameState.getChildren()

    if children == []:
        return gameState.evaluate()
    
    if maximizingPlayer:
        for child in children:
            alpha = max(alpha, alphabetaValue(child, depth-1, alpha, beta, False))
            if beta <= alpha:
                break   
        return alpha
    else:
        for child in children:
            beta = min(beta, alphabetaValue(child, depth-1, alpha, beta, True))
            if  beta <= alpha:
                break
        return beta

def alphabetaDecision(gameState, depth):
    # gameState: current game state
    # depth: search depth
    
    # return: gameState with highest value
    
    if depth < 1:
        raise Exception('Depth must be at least 1.')
    
    global nodesExamined
    nodesExamined = 0

    bestValue = -float('inf')
    bestMove = None
    
    children = gameState.getChildren()
    alpha = float('-inf')
    beta = float('inf')
    # ab pruning will always end up examining all of the children
    # immediate children of the first node
    for child in children:
        val = alphabetaValue(child, depth-1, alpha, beta, False)

        # By testing if bestMove is true, this will always select a
        # move, even when none are better than -inf. It shouldn't
        # change any decisions, except for the cases where all moves
        # have value -inf.
        if not bestMove or val > bestValue:
            bestValue = val
            bestMove = child
    
    return bestMove, nodesExamined
        
    
