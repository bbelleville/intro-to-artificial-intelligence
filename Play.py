from TicTacToe import *
from Minimax import *
from ABPruning import *

def playttt(depth = 9, algorithm = minimaxDecision, heuristic = hw_heuristic):
    playAgain = True

    while playAgain:

        start = TicTacToe.initialBoard(heuristic = heuristic)
        start.draw()

        print("Generating first move...")
        aiMove = algorithm(start, depth)
        aiMove.draw()

        while True:
            if aiMove.isWinner('X'):
                print("You lose.")
                break
            elif aiMove.isWinner('O'):
                print("You win.")
            elif aiMove.isDraw():
                print("Draw")
                break

            userMove = int(input("Enter your move: "))
            userBoard = list(aiMove.board)
            while userBoard[userMove] != ' ':
                print("Move already taken")
                userMove = int(input('Enter your move [0 - 8]: '))
                userBoard = list(aiMove.board)

            userBoard[userMove] = 'O'

            # match the users move with the states children
            children = aiMove.getChildren()
            for move in children:
                if userBoard == move.board:
                    newMove = move
                    break

            newMove.draw()

            if abs(aiMove.evaluate()) == float('inf'):
                print("game over")
                break

            aiMove = algorithm(newMove, depth)
            aiMove.draw()

        question = input('Do you want to play again? ')
        if question not in ('Yes yes y Y'):
            playAgain = False


if __name__ == "__main__":
    playttt()
