from GameState import *

useAnsiColors = False

class ConnectFour(GameState):
    # Values used to represent the different possible positions of the board
    BLACK = 1
    RED = 2

    def __init__(self, board, rows, nextPlayer):
        super(ConnectFour, self).__init__()
        self.board = board
        self.nextPlayer = nextPlayer
        self.rows = rows

    @staticmethod
    def initialBoard(rows = 6, columns = 7):
        if rows < 4:
            raise ValueError("rows must be greater than or equal to 4")
        if columns < 4:
            raise ValueError("columns must be greater than or equal to 4")
        ib = [[]] * columns
        return ConnectFour(ib,rows, ConnectFour.BLACK)

    def getChildsNextPlayer(self):
        if self.nextPlayer == ConnectFour.BLACK:
            return ConnectFour.RED
        else:
            return ConnectFour.BLACK

    def deepCopyBoard(self):
        nb = []
        for column in self.board:
            nb.append(list(column))
        return nb

    def generateChildren(self):
        if not self.hasChildren():
            return []

        c = []
        for i in range(0, len(self.board)):
            if len(self.board[i]) < self.rows:
                child = self.deepCopyBoard()

                child[i].append(self.nextPlayer)
                c.append(ConnectFour(child, self.rows, self.getChildsNextPlayer()))
        return c

    def winner(self):
        val = self.heuristic()
        if val == float('inf'):
            return self.BLACK
        if val == float('-inf'):
            return self.RED
        return False

    def isWinningState(self):
        val = self.heuristic()
        if val == float('inf') or val == float('-inf'):
            return True
        return False

    def isBoardFull(self):
        for column in self.board:
            if len(column) < self.rows:
                return False
        return True

    def hasChildren(self):
        return not(self.isWinningState() or self.isBoardFull())

    # Draw can use ansi color codes to make the output red. See
    # http://support.microsoft.com/kb/101875 for how to enable support
    # on windows
    def draw(self):
        # create format string
        fmtList = ["|{}"] * len(self.board)
        fmtList.append("|")
        fmtString = str.join('', fmtList)
        lineSep = str.join('', ['-'] * (len(self.board)*2 + 1))
        # draw from the top down
        for i in range(0, self.rows):
            fmtArgs = []
            index = self.rows - (i + 1)
            for column in self.board:
                try:
                    if column[index] == self.RED:
                        if useAnsiColors:
                            fmtArgs.append("\x1b[31mR\x1b[0m")
                        else:
                            fmtArgs.append("R")
                    else:
                        fmtArgs.append("B")
                except:
                    fmtArgs.append(" ")
            print(str.format(fmtString, *fmtArgs))
            print(lineSep)

    def evaluate(self):
        return self.heuristic()

    def heuristic(self):
    # score: +inf for Black win
    #        6 for 3 Black in line without Red
    #        3 for 2 Black in line without Red
    #        1 for 1 Black in line without Red
    #        -inf for Red win
    #        -6 for 3 Red in line without Black
    #        -3 for 2 Red in line without Black
    #        -1 for 1 Red in line without Black
        def collectFrom(pos, offsets):
            rval = []
            x,y = pos
            for dx,dy in offsets:
                nx = x + dx
                ny = y + dy
                # bounds check
                if nx >= 0 and nx < len(self.board) and ny >= 0 and ny < len(self.board[nx]):
                    rval.append(self.board[nx][ny])
            return rval

        total = 0;
        
        horizontal = [[0,0], [1,0],[2,0],[3,0]]
        vertical = [[0,0], [0,1],[0,2],[0,3]]
        upDia = [[0,0], [1,1],[2,2],[3,3]]
        downDia = [[0,0], [1,-1],[2,-2],[3,-3]]

        offsets = [ horizontal, vertical, upDia, downDia ]

        for x in range(0,len(self.board)):
            for y in range(0,self.rows):
                for o in offsets:
                    line = collectFrom((x,y), o)
                    if line.count(self.RED) == 0:
                        count = line.count(self.BLACK)
                        if count == 1:
                            total += 1
                        elif count == 2:
                            total += 3
                        elif count == 3:
                            total += 6
                        elif count == 4:
                            return float('inf')
                    elif line.count(self.BLACK) == 0:
                        count = line.count(self.RED)
                        if count == 1:
                            total -= 1
                        elif count == 2:
                            total -= 3
                        elif count == 3:
                            total -= 6
                        elif count == 4:
                            return float('-inf')
                            
        return total

        
    
