# ttt.py: modeling of tic tac toe

from GameState import *

# Heuristics to evaluate the game state
def inf_heuristic(state):
    # winning moves for O receive a score of -inf, winning moves
    # for X receive a score of +inf

    options = [['X', float('inf')],['O', float('-inf')]]
    for xx in range(0,2):
        player,score = options[xx]

        # test rows
        if all(x == player for x in state.board[0:3]):
            return score
        if all(x == player for x in state.board[3:6]):
            return score
        if all(x == player for x in state.board[6:9]):
            return score

        # test columns
        for i in range(0,3):
            c = []
            for j in range(0,3):
                c.append(state.board[i + (3*j)])
            if all(x == player for x in c):
                return score

        # test diagonals
        if all(x == player for x in [state.board[i] for i in (0,4,8)]):
            return score
        if all(x == player for x in [state.board[i] for i in (2,4,6)]):
            return score

    # else return 0
    return 0

def hw_heuristic(state):
    # score: 3 for 2 X in line without O
    #        1 for 1 X in line without O
    #        -3 for 2 O in line without X
    #        -1 for 1 O in line without X
    # define indices for each pattern
    # horizontal
    top = [0, 1, 2]
    mid = [3, 4, 5]
    bot = [6, 7, 8]

    # vertical
    lef = [0, 3, 6]
    cen = [1, 4, 7]
    rig = [2, 5, 8]

    # diagonal
    dow = [0, 4, 8]
    up  = [2, 4, 6]

    indices = [top, mid, bot, lef, cen, rig, dow, up]

    score = 0

    for line in indices:
        line_contents = [state.board[x] for x in line]
        if line_contents.count('O') == 3:
            return -100
        elif line_contents.count('X') == 3:
            return 100

        if line_contents.count('O') == 0:
            if line_contents.count('X') == 1:
                score += 1
            elif line_contents.count('X') == 2:
                score += 3
        elif line_contents.count('X') == 0:
            if line_contents.count('O') == 1:
                score -= 1
            elif line_contents.count('O') == 2:
                score -= 3

    return score

def depthHeuristic(state, depth):
    # 10 - depth for loss
    # -10 - depth for loss
    # 0 for neither
    # depth : number of moves from current move
    
    if state.isWinner('O'):
        # computer loses
        return -10 + depth
    elif state.isWinner('X'):
        # computer wins
        return 10 + depth
    else:
        return 0
        
# class to represent a tic-tac-toe gamestate
#
# the board is represented as a list of 9 elements, they can either
# be 'X', 'O', or ' ' (empty)
class TicTacToe(GameState):
    def __init__(self, board, nextPlayer, heuristic):
        super(TicTacToe, self).__init__()
        self.board = board
        self.nextPlayer = nextPlayer
        self.heuristic = heuristic

    @staticmethod
    def initialBoard(heuristic = hw_heuristic):
        ib = [' '] * 9
        im = 'X'
        return TicTacToe(ib, im, heuristic)

    def generateChildren(self):
        c = []
        if not self.hasChildren():
            return c

        if self.nextPlayer == 'X':
            np_child = 'O'
        else:
            np_child = 'X'

        for i in range(0, len(self.board)):
            if self.board[i] == ' ':
                nb = list(self.board)
                nb[i] = self.nextPlayer
                c.append(TicTacToe(nb,np_child, self.heuristic))
        return c

    def evaluate(self):
        # wrapper for heuristic functions
        return self.heuristic(self)

    def hasChildren(self):
        if self.board.count(' ') == 0 or self.isWinner('X') or self.isWinner('O'):
            return False
        return True

    def isDraw(self):
        if self.board.count(' ') == 0:
            return True
        return False

    def draw(self):
        print("\n",self.board[0],"|",self.board[1],"|",self.board[2],sep='')
        print("-----",sep='')
        print(self.board[3],"|",self.board[4],"|",self.board[5],sep='')
        print("-----",sep='')
        print(self.board[6],"|",self.board[7],"|",self.board[8], "\n",sep='')
    
    def isWinner(self, icon):
        # evaluate if player with the given icon is a winner
        top = [0, 1, 2]
        mid = [3, 4, 5]
        bot = [6, 7, 8]
    
        # vertical
        lef = [0, 3, 6]
        cen = [1, 4, 7]
        rig = [2, 5, 8]
    
        # diagonal
        dow = [0, 4, 8]
        up  = [2, 4, 6]
    
        lines = [top, mid, bot, lef, cen, rig, dow, up]
        
        for line in lines:
            line_contents = [self.board[x] for x in line]
            if line_contents.count(icon) == 3:
                return True
        return False
            
    
         
