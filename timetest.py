from time import time
from sys import argv
from ConnectFour import *
from Minimax import *
from ABPruning import *

# This is a simple test to compare the runtime of minimax and alpha
# beta pruning. It uses python's time function, which probably isn't
# very accurate, but it provides a starting point for our evaluations.
def timetest(depth = 4):
    t1 = time()
    alphabetaDecision(ConnectFour([[1], [1,1], [2], [2], [], [], []], 6, 1), depth)
    t2 = time()
    print(t2 - t1)

    t3 = time()
    minimaxDecision(ConnectFour([[1], [1,1], [2], [2], [], [], []], 6, 1), depth)
    t4 = time()

    print(t4 - t3)

if __name__ == "__main__":
    if len(argv) == 2:
        timetest(int(argv[1]))
    else:
        timetest()
