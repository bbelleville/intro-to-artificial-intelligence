from ConnectFour import *
from Minimax import *
from ABPruning import *

def play(algorithm = minimaxDecision, depth = 4, rows = 6, columns = 7):
    playAgain = True
    while playAgain:

        start = ConnectFour.initialBoard(rows = rows, columns = columns)
        start.draw()

        print("Generating first move...")
        aiMove = algorithm(start, depth)
        print("")
        aiMove.draw()

        while True:
            if not aiMove.hasChildren():
                winner = aiMove.winner()
                if winner == ConnectFour.BLACK:
                    print("You lose.")
                    break
                elif winner == ConnectFour.RED:
                    print("You win.")
                    break
                else:
                    print("Draw")
                    break

            for i in range(0, columns):
                print("",i,end="")
            print("")               
            userMove = int(input("Enter your move: "))
            userBoard = aiMove.deepCopyBoard()
            while len(userBoard[userMove]) >= aiMove.rows:
                print("Column full")
                userMove = int(input('Enter your move : '))
                

            userBoard[userMove].append(ConnectFour.RED)

            # match the users move with the states children
            children = aiMove.getChildren()
            for move in children:
                if userBoard == move.board:
                    newMove = move
                    break

            print("")
            newMove.draw()

            if not newMove.hasChildren():
                winner = newMove.winner()
                if winner == ConnectFour.BLACK:
                    print("You lose.")
                    break
                elif winner == ConnectFour.RED:
                    print("You win.")
                    break
                else:
                    print("Draw")
                    break


            aiMove = algorithm(newMove, depth)
            print("")
            aiMove.draw()

        question = input('Do you want to play again? ')
        if question not in ('Yes yes y Y'):
            playAgain = False

if __name__ == "__main__":
    play()
