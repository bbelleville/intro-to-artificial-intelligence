def alphabetaValue(gameState, depth, alpha, beta, maximizingPlayer, recursionDepth = 1):
    # gameState: game state to be evaluated
    # depth: search depth
    # alpha: maximum score, initially -Inf
    # beta: minimum score, initiall Inf
    # maximizingPlayer: True if maximizing player's turn

    # return: value of game state

    # Recursion has reached the search depth or a terminal node
    if depth == 0:
        return gameState.evaluate() # Not yet implemented
   
    if not gameState.hasChildren():
        if gameState.isWinner('X'):
            return gameState.evaluate() - recursionDepth
        elif gameState.isWinner('O'):
            return gameState.evaluate() + recursionDepth
        return gameState.evaluate()
    
    children = gameState.generateChildren()

    if maximizingPlayer:
        children = gameState.getChildren()
        for child in children:
            alpha = max(alpha, alphabetaValue(child, depth-1, alpha, 
                                              beta, False, recursionDepth+1)
                        -recursionDepth)
            #===================================================================
            # child.draw()
            # print('Alpha:', alpha)
            #===================================================================
            if beta <= alpha:
                #===============================================================
                # print('PRUNING AT:')
                # child.draw()
                # print('The score is:', child.evaluate())
                # print('alpha:', alpha, 'beta:', beta)
                #===============================================================
                break   
        return alpha
    else:
        children = gameState.getChildren()
        for child in children:
            beta = min(beta, alphabetaValue(child, depth-1, alpha, 
                                            beta, True, recursionDepth+1)
                       + recursionDepth)
            #===================================================================
            # child.draw()
            # print('Beta:', beta)
            #===================================================================
            if  beta <= alpha:
                #===============================================================
                # print('PRUNING AT:')
                # child.draw()
                # print('The score is:', child.evaluate())
                # print('alpha:', alpha, 'beta:', beta)
                #===============================================================
                break
        return beta

def alphabetaDecision(gameState, depth):
    # gameState: current game state
    # depth: search depth
    
    # return: gameState with highest value

    bestValue = -float('inf')
    bestMove = None
    
    children = gameState.getChildren()
    for child in children:
        val = alphabetaValue(child, depth-1, float('-inf'), float('inf'), False)
        child.draw()
        print("This is a child of the current node.\nIts ABPruning value:", val)
        if  val > bestValue:
            bestMove = child
            bestValue = val
            
    return bestMove
        
    
