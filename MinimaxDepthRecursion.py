def minimaxValue(gameState, depth, maximizingPlayer, recursionDepth = 1):
    # gameState: game state to be evaluated
    # depth: search depth
    # maximizingPlayer: True if maximizing player's turn

    # return: value of game state

    # Recursion has reached the search depth or a terminal node
    if depth == 0:
        return gameState.evaluate()
    
    if not gameState.hasChildren():
        if gameState.isWinner('X'):
            return gameState.evaluate() - recursionDepth
        elif gameState.isWinner('O'):
            return gameState.evaluate() + recursionDepth
        return gameState.evaluate()
    
    children = gameState.generateChildren()
    
    if maximizingPlayer:
        bestVal = -float('inf')
        for child in children:
            val = minimaxValue(child, depth-1, False, recursionDepth+1)
            #------------------------------------------- if recursionDepth == 1:
                #--------------------- print("Recursion depth:", recursionDepth)
                #------------------------------------------- print("Max's turn")
                #-------------------------------------------------- child.draw()
                #------------------------------------ print("score:", val, "\n")
            if val > 0:
                recursionDepth = -recursionDepth
            bestVal = max(bestVal, val + recursionDepth)
        return bestVal
    else:
        bestVal = float('inf')
        for child in children:
            val = minimaxValue(child, depth-1, True, recursionDepth+1)
            #------------------------- print("Recursion depth:", recursionDepth)
            #----------------------------------------------- print("Min's turn")
            #------------------------------------------------------ child.draw()
            #---------------------------------------- print("score:", val, "\n")
            if val > 0:
                recursionDepth = -recursionDepth
            bestVal = min(bestVal, val + recursionDepth)
        return bestVal

def minimaxDecision(gameState, depth):
    # gameState: current game state
    # depth: search depth
    
    # return: gameState with highest value

    bestValue = -float('inf')
    bestMove = None
    
    children = gameState.getChildren()
    for child in children:
        val = minimaxValue(child, depth-1, False)
        child.draw()
        print('Minimax value:', val)
        if val > bestValue:
            bestMove = child
            bestValue = val

    return bestMove
        
    
