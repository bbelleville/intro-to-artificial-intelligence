from ABPruning import *
from Minimax import *
from ConnectFour import *
from TicTacToe import *

# test if both algorithms select the same move from given board 
# for all recursion depths
def algorithmsMatch(board):
    for depth in range(9):

        minimaxMove = minimaxDecision(board, depth)
        alphabetaMove = alphabetaDecision(board, depth)
        
        if minimaxMove.board != alphabetaMove.board:
            print('Starting from board:')
            board.draw()
            print('Minimax and ABPruning do not match at depth', depth)
            print('Minimax selected:')
            minimaxMove.draw()
            print('ABPruning selected:')
            alphabetaMove.draw()
            return False
        elif depth == 8:
            return True

# determineTurn is used to reconstruct the TicTacToe object
# from a list representation of the board
def determineTurn(board):
    # assuming X goes first
    if board.count('X') <= board.count('O'):
        return 'X'
    return 'O'

# Returns a TicTacToe object from a list-represented board
def listToTTT(board):
    return TicTacToe(board, determineTurn(board), hw_heuristic)
            
def generateAllBoards():
    # returns a list of all unique non-terminal list-represented boards
    initialBoard = TicTacToe.initialBoard()
    allBoards = []
    for child in initialBoard.generateChildren():
        allBoards.append(child.board)
        
    for board in allBoards:
        board = listToTTT(board)
        
        if board.hasChildren():
            for child in board.generateChildren():
                if child.board not in allBoards and child.hasChildren():
                    allBoards.append(child.board)
                    
        print('Number of unique non-terminal boards:', len(allBoards))
        
    return allBoards

def testTicTacToe():
    # starts out slowly as the algorithms have more positions
    # to test in the emptier boards
    allBoards = generateAllBoards()
    
    i = 1
    n = len(allBoards)
    for board in allBoards:
        print('Testing', i, 'of', n, 'boards.')
        algorithmsMatch(listToTTT(board))
        i+= 1


