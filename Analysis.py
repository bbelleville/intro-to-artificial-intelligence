from TicTacToe import *
from ConnectFour import *
from Minimax import *
from ABPruning import *
import time

'''
    Edit compare function at the bottom to compare Connect Four or Tic-Tac-Toe
'''

def compareAlgs(board, depth):
    # Returns the counts of nodes examined and time
    t0 = time.time()
    minimaxNodesExamined = minimaxDecision(board, depth)[1]
    t1 = time.time()
    abNodesExamined = alphabetaDecision(board, depth)[1]
    t2 = time.time()
    
    minimaxElapsedTime = round(t1 - t0, 3)
    abElapsedTime = round(t2 - t1, 3)
    
    return minimaxNodesExamined, abNodesExamined, minimaxElapsedTime, abElapsedTime

def compareTTT(depth):
    # Prints out statistical comparisons for TicTacToe
    
    gameStateHW = TicTacToe.initialBoard(hw_heuristic)
    gameStateInf = TicTacToe.initialBoard(inf_heuristic)
    
    t0 = time.time()
    minimaxNodesExaminedHW = minimaxDecision(gameStateHW, depth)[1]
    t1 = time.time()
    abNodesExaminedHW  = alphabetaDecision(gameStateHW, depth)[1] 
    t2 = time.time()
    
    minimaxNodesExaminedInf = minimaxDecision(gameStateInf, depth)[1]
    t3 = time.time()
    
    abNodesExaminedInf  = alphabetaDecision(gameStateInf, depth)[1]
    t4 = time.time()
    
    print('Search depth =',depth, '#'*25)
    
    print('HW heuristic')
    print('Minimax nodes examined:', minimaxNodesExaminedHW)
    print('Time elapsed:', round(t1-t0, 3), 'seconds')
    print('Alpha-beta pruning nodes examined:', abNodesExaminedHW)
    print('Time elapsed:', round(t2-t1, 3), 'seconds')
    print('% Nodes examined:', round(100*float(abNodesExaminedHW)/(minimaxNodesExaminedHW),2), '\n')
      
    print('Infinity heuristic')
    print('Minimax nodes examined:', minimaxNodesExaminedInf)
    print('Time elapsed:', round(t3-t2, 3), 'seconds')     
    print('Alpha-beta pruning nodes examined:', abNodesExaminedInf)
    print('Time elapsed:', round(t4-t3, 3), 'seconds')
    print('% Nodes examined:', round(100*float(abNodesExaminedInf)/(minimaxNodesExaminedInf),2))
    print('\n')

def compareCF(depth):
    # At depth = 6, minimax takes 45.6 seconds, abpruning = 14.6 seconds, 41.17% of nodes
    # Depth = 7, minimax takes 321.9 seconds, abpruning = 62.017 seconds, only 25.6% of nodes
    # DO NOT TRY TO RUN DEPTH > 7 WITH MINIMAX
    connectFourInitialBoard = ConnectFour.initialBoard()
    stats = compareAlgs(connectFourInitialBoard, depth)
    
    print('Connect 4, Depth =', depth, '#' * 25)
    print('Minimax nodes examined:', stats[0])
    print('Elapsed time:', stats[2], 'seconds\n')
    print('Alpha-beta pruning nodes examined:', stats[1])
    print('Elapsed time:', stats[3], 'seconds')
    print('% Nodes Examined:', round(100*float(stats[1])/stats[0], 2))
    print('\n')
    

if __name__== "__main__":
    for depth in range(1,10):
        compareTTT(depth)
        
    #===========================================================================
    # for depth in range(1,7):
    #     compareCF(depth)
    #===========================================================================
        
    